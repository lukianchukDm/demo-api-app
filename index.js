const express    = require("express")
const bodyParser = require("body-parser")
const fs = require("fs")
const _  = require("lodash")
const moment = require("moment")
var cors = require('cors')


const app = express()

app.use(bodyParser.json())
app.use(cors())

const authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
const studentsJsonFile = "students.json"
let startTimestamp

const displayHomepageMessage = (req, res) => {
    res.sendFile("home.html", { root: "." })
}

const displayInstructions = (req, res) => {
    res.sendFile("index.html", { root: "." })
}

const healthcheck = (req, res) => {
    const healthcheck = {
        message: 'OK',
        uptime: Date.now() - startTimestamp // use moment
    }

    try {
        res.status(200).send(healthcheck);
    } catch (e) {
        healthcheck.message = e
        res.status(503).send(healthcheck)
    }
}

const getStudents = (req, res) => {
    const students = getFile(studentsJsonFile)
    res.status(200).send(students)
}

const getStudent = (req, res) => {
    const id = req.params.studentId
    const { student } = getStudentById(id, res)

    res.status(200).json(student)
}

const createStudent = (req, res) => {
    const { fullname, deposit, group, classes } = req.body

    if (!checkAuth(req, res)) return

    if (!fullname || fullname.length == 0 || !Array.isArray(classes) || classes.length == 0) {
        res.status(400).send({ message: "Some required fields are missing or empty (fullname and/or classes). Please refer to guidelines at /api for more details." })
        return
    }

    let isPaid = false
    let amount  = undefined
    if (!!deposit) {

        if (typeof deposit.isPaid !== "boolean") {
                res.status(422).send({ message: "isPaid should be boolean" })
                return
            }

        isPaid = deposit.isPaid || false
        amount  = (isPaid) ? deposit.amount || res.status(400).send({ message: "If deposit is paid, please specify the amount" }) : undefined
    }

    let { registrationDate } = req.body
    registrationDate = assignRegistrationDate(registrationDate, res)
    if (!registrationDate) return

    if (!areClassesValid(classes, res)) return

    let students = getFile(studentsJsonFile)

    let student = {
        fullname,
        registrationDate,
        deposit: {
            isPaid,
            amount
        },
        group,
        classes,
        id: assignId(students)
    }

    students.push(student)

    saveFile(studentsJsonFile, students)
    res.status(201).send(student)
}

const updateStudent = (req, res) => {
    if (!checkAuth(req, res)) return

    const response = updateStudentFromRequest(req, res)
    if (!response) return
    const { student, index } = response

    const students  = getFile(studentsJsonFile)
    students[index] = student

    saveFile(studentsJsonFile, students)
    res.status(200).send(student)
}

const replaceStudent = (req, res) => {
    if (!checkAuth(req, res)) return

    const response = updateStudentFromRequest(req, res)
    if (!response) return
    const { student, index } = response

    const students = getFile(studentsJsonFile)

    student.id = assignId(students)

    let { registrationDate } = req.body
    student.registrationDate = assignRegistrationDate(registrationDate, res)
    if (!student.registrationDate) return

    students.push(student)
    students.splice(index, 1)

    saveFile(studentsJsonFile, students)
    res.status(201).send(student)
}

const removeStudent = (req, res) => {
    if (!checkAuth(req, res)) return

    let id = req.params.studentId
    let { student, index } = getStudentById(id, res)

    let students = getFile(studentsJsonFile)
    students.splice(index, 1)

    saveFile(studentsJsonFile, students)
    res.status(200).send({ message: `Student with ID ${id} has been removed.`})
}







app.get("/", displayHomepageMessage)
app.get("/api", displayInstructions)
app.get("/healthcheck", healthcheck)

app.route("/students")
    .get(getStudents)
    .post(createStudent)

app.route("/students/:studentId")
    .get(getStudent)
    .patch(updateStudent)
    .put(replaceStudent)
    .delete(removeStudent)

app.listen(process.env.PORT || 5000, () => {
    seedData()
    startTimestamp = Date.now()
    console.log(`Server is running on port 5000.`)

    setInterval(() => {
        seedData()
    }, 600000)
})







const seedData = () => {
    let file = getFile("seed.json")
    saveFile(studentsJsonFile, file)
    console.log("Data has been seeded")
}

const getFile = (filename) => {
    var data = fs.readFileSync(filename, "ascii")
    return JSON.parse(data)
}

const saveFile = (filename, file) => {
    var sorted = file.slice(0)
    sorted.sort(function(a, b) {
        return a.id - b.id
    })

    let jsonData = JSON.stringify(sorted, null, 4)
    fs.writeFileSync(filename, jsonData)
}

const checkAuth = (req, res) => {
    var token = req.headers['x-access-token']

    if (!token) {
        res.status(401).send({ auth: false, message: "No token provided." })
        return false
    }

    if (token != authToken) {
        res.status(401).send({ auth: false, message: "Token is not valid." })
        return false
    }

    if (token == authToken) return true
}

const getStudentById = (id, res) => {
    if (Number.isInteger(Number.parseInt(id))) {

        const students = getFile(studentsJsonFile)

        for (let i = 0; i < students.length; i++) {
            if (students[i].id == id) {
                return {
                    student: students[i],
                    index:   i
                }
            }
        }

        res.status(404).send({ message: `Student with ID ${id} is not found.` })

        return false

    } else res.status(422).send({ message: "Student ID should be an integer" })

    return false
}

const assignId = (students) => {
    let ids = []

    for (let i = 0; i < students.length; i++) {
        ids.push(students[i].id)
    }

    var n = ids.length;
    var total = ((n) * (n + 1)) / 2;
    for (let i = 0; i < n; i++) {
        total -= ids[i];
    }

    if (total < 0) return 0
    else return total
}

const assignRegistrationDate = (registrationDate, res) => {
    if (registrationDate) {
        const dateFormatRegex = /(^20\d{2})-(1[0-2]|0[1-9])-(3[0-1]|[1-2][0-9]|0[1-9])$/

        if (!dateFormatRegex.test(registrationDate)) {
            res.status(422).send({ message: "Registration date is in wrong format. Expected format is `YYYY-MM-DD`. Please refer to guidelines at /api for more details." })
            return false
        } else return registrationDate

    } else return moment().format('YYYY-MM-DD')
}

const updateStudentFromRequest = (req, res) => {
    if (_.isEmpty(req.body)) {
        res.status(400).send({ message: "Please provide at least 1 parameter to update."})
        return
    }

    const { fullname, deposit, group, classes } = req.body

    let id = req.params.studentId
    let { student, index } = getStudentById(id, res)

    if (fullname) student.fullname = fullname
    if (fullname.length == 0) {
        res.status(400).send({ message: "Fullname should not be empty. Please refer to guidelines at /api for more details." })
        return
    }

    if (!Array.isArray(classes)) {
        res.status(422).send({ message: "Classes should be an array of strings. Please refer to guidelines at /api for more details." })
        return
    }

    if (classes) {
        if (classes.length == 0) {
            res.status(400).send({ message: "Classes should not be empty. Please refer to guidelines at /api for more details." })
            return
        }

        else if (!areClassesValid(classes, res)) return

        else student.classes = classes
    }

    if (!!deposit) {

        let isPaid

        if (deposit.isPaid == undefined) {

            if (student.deposit.isPaid == undefined) isPaid = false
            else student.deposit.isPaid

        }

        else {
            if (typeof deposit.isPaid === "boolean") isPaid = deposit.isPaid
            else {
                res.status(422).send({ message: "isPaid should be boolean" })
                return
            }
        }

        let amount  = (isPaid) ? deposit.amount || student.deposit.amount || res.status(400).send({ message: "If deposit is paid, please specify the amount" }) : undefined
        student.deposit = { isPaid, amount }
    }

    if (group) student.group = group

    return { student, index }
}

const areClassesValid = (classes, res) => {
    const arr = [ "manual", "automation", "api" ]

    for (let i = 0; i < classes.length; i++) {
        if (!arr.includes(classes[i])) {
            res.status(422).send({ message: "Unknown class. Available options are 'manual', 'automation', 'api'. Please refer to guidelines at /api for more details." })
            return false
        }
    }

    return true
}